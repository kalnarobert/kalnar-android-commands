#!/bin/bash

function __android_device__() {
  DEVICES=$(adb devices | tail -n +2 | sed '$d' | cut -f1)
  DEV_NUM=$(echo $DEVICES | wc -w)
  if [[ $DEV_NUM -eq 0 ]]; then
    echo "no device connected" 1>&2; return
  elif [[ $DEV_NUM -eq 1 ]]; then
    DEVICE=$DEVICES
  else 
    DEVICE=$(adb devices -l | tail -n +2 | sed '$d' | tr -s ' ' | fzf | cut -f1 -d" ")
  fi
  echo $DEVICE
}

function androidAnimation() {
  NUMBER_REGULAR_EXP='^[01]+([.][0-9]+)?$'
  if ! [[ $1 =~ $NUMBER_REGULAR_EXP ]] ; then
     echo "error: argument should be between 0 and 1"; return
  fi
  FACTOR=$1
  DEVICE=$(__android_device__)
  if [[ $DEVICE -eq "" ]]; then
    return
  fi
  set -x
  adb -s $DEVICE shell settings put global window_animation_scale $FACTOR;
  adb -s $DEVICE shell settings put global transition_animation_scale $FACTOR;
  adb -s $DEVICE shell settings put global animator_duration_scale $FACTOR
  set +x
}

function androidScreenShot() {
  if [[ $1 == '' ]] ; then
     echo "error: argument output file name was not specified"; return
  fi
  DEVICE=$(__android_device__)
  if [[ $DEVICE -eq "" ]]; then
    return
  fi
  set -x
  adb -s $DEVICE exec-out screencap -p > $1
  set +x
}

